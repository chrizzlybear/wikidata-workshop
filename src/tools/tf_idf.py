#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import spacy
import pprint

from math import log
from collections import Counter
from operator import itemgetter

from tools.lemmatize import Lemmatizer

max_arts = 500


class tfidf:

    def __init__(self):
        self.Lemmatizer = Lemmatizer()

        self.article_cnt = 0

        # each list element is a dict with one key per found non-stopword
        # lemma and the wordcount as corresponding value
        self.lemma_count_list = []

        # counts one lemma occurance per document
        self.corpus_lemma_occurances = dict()
        # contains the idf for each lemma in the corpus
        self.idf_dict = dict()
        # each element is a dict with lemmas as keys and their tfidf as value
        self.tfidf_list = []

    def add_csv(self, path):

        self.df = pd.read_csv(path)

    def add_article(self, sent):

        # increment article counter
        self.article_cnt += 1

        # create a list of lemmas from the sentence string
        lemmas = self.Lemmatizer.lemmatize(text=sent)
        # count each occurance
        lemma_counts = dict(Counter(lemmas))

        # append lemma count dict to the count list
        self.lemma_count_list.append(lemma_counts)

        # for each lemma: add on to the occurance counter or create
        # key if lemma occurs the first time
        for key, value in lemma_counts.items():
            if key in self.corpus_lemma_occurances:
                self.corpus_lemma_occurances[key] += 1
            else:
                self.corpus_lemma_occurances[key] = 1

    def calculate_idf(self):

        for lemma, count in self.corpus_lemma_occurances.items():
            self.idf_dict[lemma] = log(self.article_cnt / count + 1)

    def calculate_tf_idf(self):

        for article in self.lemma_count_list:
            tfidf_dict = dict()

            # calculate the tfidf for each lemma in the article and append it to the list
            for lemma, count in article.items():
                tfidf_dict[lemma] = round(count * self.idf_dict[lemma], ndigits=2)
            self.tfidf_list.append(tfidf_dict)

    def run_tfizer(self):

        # make a list with each sentence becoming one item
        self.article_list = self.df["description"].head(max_arts).tolist()

        for article in self.article_list:
            self.add_article(article)

        self.calculate_idf()
        self.calculate_tf_idf()

        for article in self.tfidf_list:
            pprint.pprint(
                sorted(article.items(), key=itemgetter(1), reverse=True))
