#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import spacy
import string

# print(string.punctuation)


class Lemmatizer():
    def __init__(self):
        self.nlp = spacy.load("de")
        self.punctuation = string.punctuation +"„“–"

    def lemmatize(self, text):
        lemma_list = []
        for token in self.nlp(text):
            if len(token.text) == 1:
                if token.text in self.punctuation:
                    continue
            if not token.is_stop:
                lemma_list.append(token.lemma_)

        return lemma_list
