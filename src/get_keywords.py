#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from tools import tf_idf


if __name__ == '__main__':
    tf = tf_idf.tfidf()
    tf.add_csv("../mobile-opferberatung-scraper.csv")
    tf.run_tfizer()
